package it.uniba.di.sms.gruppo25.dibapp.course;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.C;
import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.lesson.LessonListAdapter;
import it.uniba.di.sms.gruppo25.dibapp.model.Course;
import it.uniba.di.sms.gruppo25.dibapp.model.Lesson;

import static android.content.Context.MODE_PRIVATE;


public class CourseDetailFragment extends Fragment {

    private static final String COURSE_ID = "course";
    private static final String IS_TEACHER = "isTeacher";
    private static final String TEACHER_NAME = "teacherName";

    private String courseId;
    private boolean isTeacher;
    private String teacherName;
    private Course course;
    private TextView txtName;
    private TextView txtDesc;
    private TextView txtTeacher;
    private TextView txtNumberOfLessons;
    private TextView txtNumberOfPresences;

    private int numberOfLessons;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private LessonListAdapter adapter;
    private FirebaseDatabase db;
    private String emptyListMessage;
    private ConstraintLayout constraintLayout;
    private int i=0;
    private Button btnSubscribe, btnUnsubscribe;



    public CourseDetailFragment() {
        // Required empty public constructor
    }

    public static CourseDetailFragment newInstance(String courseId, boolean isTeacher, String teacherName) {
        CourseDetailFragment fragment = new CourseDetailFragment();
        Bundle args = new Bundle();
        args.putString(COURSE_ID, courseId);
        args.putBoolean(IS_TEACHER, isTeacher);
        args.putString(TEACHER_NAME, teacherName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        final View view = inflater.inflate(R.layout.fragment_course_detail, container, false);
        //view.findViewById(R.id.course_detail_content).setVisibility(View.GONE);
        txtName = view.findViewById(R.id.txt_course_name);
        txtDesc = view.findViewById(R.id.txt_course_desc);
        txtTeacher = view.findViewById(R.id.txt_course_teacher);
        txtNumberOfLessons = view.findViewById(R.id.txt_course_n_of_lessons);
        txtNumberOfPresences = view.findViewById(R.id.txt_course_n_of_presences);
        btnSubscribe = view.findViewById(R.id.btnSubscribe);
        btnUnsubscribe = view.findViewById(R.id.btnUnsubscribe);
        courseId = getArguments().getString(COURSE_ID);
        btnUnsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unsubscribe();
            }
        });
        btnSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subscribe(view);
            }
        });
        teacherName = getArguments().getString(TEACHER_NAME);
        retrieveCourseDetail(courseId, getContext());

        if(isTeacher){
            emptyListMessage = getString(R.string.empty_lesson_list_teacher_messsage);
            txtNumberOfPresences.setVisibility(View.GONE);

        }
        else {
            emptyListMessage = getString(R.string.empty_lesson_list_student_message);
            retrieveSubscription(getContext());

        }

        return view;
    }



    private void retrieveCourseDetail(String courseId, final Context context) {
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("courses").child(courseId);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    //  getView().findViewById(R.id.course_detail_progress).setVisibility(View.GONE);
                    //getView().findViewById(R.id.course_detail_content).setVisibility(View.VISIBLE);
                    course = dataSnapshot.getValue(Course.class);
                    txtName.setText(course.name);
                    txtDesc.setText(context.getString(R.string.course_description, course.description));
                    txtTeacher.setText(context.getString(R.string.course_teacher, teacherName));

                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        FirebaseDatabase.getInstance().getReference("lessons").child(courseId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    numberOfLessons = (int) dataSnapshot.getChildrenCount();
                    txtNumberOfLessons.setText(getContext().getString(R.string.course_nof_lessons, getContext().getResources().getQuantityString(R.plurals.number_of_lessons, numberOfLessons, numberOfLessons)));

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void retrieveSubscription(final Context context) {
        final FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(auth.getUid()).child("courses").child(courseId);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (!isTeacher) {
                        btnUnsubscribe.setVisibility(View.VISIBLE);
                        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("courses").child(courseId);
                        reference.child("members").child(auth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    int numberOfPresences = dataSnapshot.getValue(Integer.class);
                                    if (getView() != null) {
                                        txtNumberOfPresences.setText(context.getString(R.string.course_your_presences, numberOfPresences , numberOfLessons));
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }

                }
                else {
                    btnSubscribe.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void subscribe(final View view) {
        final EditText input = new EditText(getContext());
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.alert_enter_password_title)
                .setView(input)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString().equals(course.password)) {
                            FirebaseAuth auth = FirebaseAuth.getInstance();
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(auth.getUid()).child("courses");
                            ref.child(courseId).setValue(true);
                            ref = FirebaseDatabase.getInstance().getReference("courses").child(courseId).child("members");
                            ref.child(auth.getUid()).setValue(0);
                            btnSubscribe.setVisibility(View.GONE);
                            btnUnsubscribe.setVisibility(View.VISIBLE);
                        }
                        else {
                            Snackbar.make(view.findViewById(R.id.course_detail_frame), R.string.error_course_password, Snackbar.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
        alertDialog.show();
    }

    private void unsubscribe() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(auth.getUid()).child("courses").child(courseId);
        ref.removeValue();
        btnUnsubscribe.setVisibility(View.GONE);
        btnSubscribe.setVisibility(View.VISIBLE);
    }


}
