package it.uniba.di.sms.gruppo25.dibapp.model;

public class Presence {
    public String nome;
    public String matricola;
    public Boolean confirmed;

    public Presence(){

    }

    public Presence(String nome, String matricola, Boolean confirmed){
        this.nome = nome;
        this.matricola = matricola;
        this.confirmed = confirmed;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public String getNome() {
        return nome;
    }

    public String getMatricola() {
        return matricola;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMatricola(String matricola) {
        this.matricola = matricola;
    }
}
