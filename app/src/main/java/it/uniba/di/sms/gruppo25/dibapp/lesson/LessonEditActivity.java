package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.net.Inet4Address;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.model.Lesson;

public class LessonEditActivity extends AppCompatActivity {
    private FirebaseDatabase db;
    private String courseId, lessonId, lessonName, lessonClass, lessonDate, lessonStart, lessonEnd, lessonDesc;
    private int req_code;
    private boolean new_lesson = true;
    Animation frombottom, fromtop;
    LinearLayout linearLayout;
    EditText et,et1,et2,et3,et4;
    Button button;
    Boolean isTeacher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_edit);
        final EditText etxtDesc, etxtClassroom, etxtDate, etxtStart, etxtEnd;
        etxtDesc = findViewById(R.id.etxt_lesson_desc);
        etxtClassroom = findViewById(R.id.etxt_lesson_classroom);
        etxtDate = findViewById(R.id.etxt_lesson_date);
        etxtStart = findViewById(R.id.etxt_lesson_start);
        etxtEnd = findViewById(R.id.etxt_lesson_end);
        SharedPreferences sharedPreferences = getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        Intent intent = getIntent();
        courseId = intent.getStringExtra("courseId");
         if ((lessonId = intent.getStringExtra("lessonId")) != null) {
            new_lesson = false;
            lessonDesc = intent.getStringExtra("lessonDesc");
            lessonName = intent.getStringExtra("lessonName");
            lessonDate = intent.getStringExtra("lessonDate");
            lessonClass = intent.getStringExtra("lessonClass");
            lessonStart = intent.getStringExtra("lessonStart");
            lessonEnd = intent.getStringExtra("lessonEnd");
            etxtDesc.setText(lessonDesc);
            etxtClassroom.setText(lessonClass);
            etxtDate.setText(lessonDate);
            etxtStart.setText(lessonStart);
            etxtEnd.setText(lessonEnd);
        }
        else {
             setTitle(getString(R.string.title_activity_lesson_create));
         }

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        et=findViewById(R.id.etxt_lesson_classroom);
        et1=findViewById(R.id.etxt_lesson_date);
        et2=findViewById(R.id.etxt_lesson_desc);
        et3=findViewById(R.id.etxt_lesson_end);
        et4=findViewById(R.id.etxt_lesson_start);

        linearLayout = findViewById(R.id.linearLesson);

        button =  findViewById(R.id.btn_lesson_save);

        et.startAnimation(fromtop);
        et1.startAnimation(fromtop);
        et2.startAnimation(fromtop);
        et3.startAnimation(fromtop);
        et4.startAnimation(fromtop);
        linearLayout.startAnimation(fromtop);
        button.startAnimation(frombottom);



        etxtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog;
                Calendar calendar = Calendar.getInstance();
                datePickerDialog = new DatePickerDialog(LessonEditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etxtDate.setText(String.format("%02d/%02d/%04d",  dayOfMonth, month+1, year));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        etxtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog;
                timePickerDialog = new TimePickerDialog(LessonEditActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        etxtStart.setText(String.format("%02d:%02d", hourOfDay, minute));
                    }
                }, 0, 0, true );
                timePickerDialog.show();
            }
        });

        etxtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog;
                timePickerDialog = new TimePickerDialog(LessonEditActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        etxtEnd.setText(String.format("%02d:%02d", hourOfDay, minute));
                    }
                }, 0, 0, true );
                timePickerDialog.show();
            }
        });

        Button btnSave = findViewById(R.id.btn_lesson_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String desc, classroom, date, start, end;
                desc = etxtDesc.getText().toString();
                classroom = etxtClassroom.getText().toString();
                date = etxtDate.getText().toString();
                start = etxtStart.getText().toString();
                end = etxtEnd.getText().toString();
                if (new_lesson)
                    createLesson(desc, classroom, date, start, end);
                else
                    editLesson(courseId, lessonId, desc, classroom, date, start, end);
            }
        });
    }

    private void createLesson(String desc, String classroom, String date, String start, String end) {
        DatabaseReference ref = db.getInstance().getReference("lessons").child(courseId).push().getRef();
        String id = ref.getKey();
        Lesson lesson = new Lesson(id, classroom, desc, date, start, end, false, false, null);
        if (validateLesson(lesson)) {
            ref.setValue(lesson).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            });
        }
    }

    private void editLesson(String courseId, String lessonId, String desc, String classroom, String date, String start, String end) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(courseId).child(lessonId);
        Lesson lesson = new Lesson(lessonId, classroom, desc, date, start, end, false, false, null);
        if (validateLesson(lesson)) {
            ref.setValue(lesson);
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    private boolean validateLesson(Lesson lesson) {
        boolean isValid = true;
        long now = new Date().getTime();
        if (lesson.date.isEmpty() || lesson.startTime.isEmpty() || lesson.endTime.isEmpty()) {
            isValid = false;
            Snackbar.make(findViewById(R.id.lesson_edit_layout), R.string.error_lesson_missing_date_time, Snackbar.LENGTH_LONG ).show();
        } else if (lesson.startTimestamp <= now) {
            isValid = false;
            Snackbar.make(findViewById(R.id.lesson_edit_layout), R.string.error_lesson_date_now, Snackbar.LENGTH_LONG ).show();
        } else if (lesson.startTimestamp >= lesson.endTimestamp) {
            isValid = false;
            Snackbar.make(findViewById(R.id.lesson_edit_layout), R.string.error_lesson_date_wrong, Snackbar.LENGTH_LONG ).show();
        }
        return isValid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
