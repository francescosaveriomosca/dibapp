package it.uniba.di.sms.gruppo25.dibapp;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.course.CourseListFragment;
import it.uniba.di.sms.gruppo25.dibapp.model.User;

public class MainNavActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private boolean isTeacher;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_my_courses);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        SharedPreferences sharedPreferences = getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);
        editUI(isTeacher);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setTitle(R.string.my_courses);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, CourseListFragment.newInstance(isTeacher, false, ""));
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_courses) {
            getSupportActionBar().setTitle(R.string.my_courses);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new CourseListFragment());
            ft.commit();
        } else if (id == R.id.nav_all_courses) {
            getSupportActionBar().setTitle(R.string.all_courses);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, CourseListFragment.newInstance(isTeacher, true, ""));
            ft.commit();
        } else if (id == R.id.nav_logout) {
            AlertDialog dialog = new AlertDialog.Builder(MainNavActivity.this)
                    .setTitle(R.string.alert_logout_title)
                    .setMessage(R.string.alert_logout_message)
                    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FirebaseAuth.getInstance().signOut();
                            ActivityCompat.finishAffinity(MainNavActivity.this);
                            finish();

                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .create();
            dialog.show();
        } else if(id == R.id.nav_edit_account){
            startActivity(new Intent(this, AccountActivity.class));

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void editUI(boolean isTeacher) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        View parent = navigationView.getHeaderView(0);
        Menu navMenu = navigationView.getMenu();
        final TextView txtUsername = parent.findViewById(R.id.txt_nav_user_name);
        TextView txtUserEmail = parent.findViewById(R.id.txt_nav_user_email);
        TextView txtUserRole = parent.findViewById(R.id.txt_nav_user_role);
        txtUserEmail.setText(user.getEmail());
        FirebaseDatabase.getInstance().getReference("users").child(auth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                txtUsername.setText(user.name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if (isTeacher) {
            navMenu.findItem(R.id.nav_all_courses).setVisible(false);
            txtUserRole.setText(R.string.teacher);
        }
        else {
            txtUserRole.setText(R.string.student);
        }
    }




}
