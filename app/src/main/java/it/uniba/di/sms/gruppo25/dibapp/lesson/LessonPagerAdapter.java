package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import it.uniba.di.sms.gruppo25.dibapp.CommentFragment;
import it.uniba.di.sms.gruppo25.dibapp.PresencesListFragment;
import it.uniba.di.sms.gruppo25.dibapp.RatingsListFragment;

public class LessonPagerAdapter extends FragmentStatePagerAdapter {
    private int numOfTabs;
    private String course_id;
    private String lesson_id;

    LessonPagerAdapter(FragmentManager fm, int numOfTabs, String course_id, String lesson_id) {
        super(fm);
        this.numOfTabs = numOfTabs;
        this.course_id = course_id;
        this.lesson_id = lesson_id;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return LessonDetailFragment.newInstance(course_id, lesson_id);
            case 1:
                return CommentFragment.newInstance(lesson_id);
            case 2:
                return RatingsListFragment.newInstance(lesson_id);
            case 3:
                return PresencesListFragment.newInstance(lesson_id);

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
