package it.uniba.di.sms.gruppo25.dibapp.course;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.ViewGroup;

import it.uniba.di.sms.gruppo25.dibapp.MemberListFragment;
import it.uniba.di.sms.gruppo25.dibapp.course.CourseDetailFragment;
import it.uniba.di.sms.gruppo25.dibapp.lesson.LessonListFragment;

public class CoursePagerAdapter extends FragmentStatePagerAdapter {
    private int numOfTabs;
    private String course_id;
    private Boolean isTeacher;
    private String teacherName;
    private int numberOfLessons;

    CoursePagerAdapter(FragmentManager fm, int numOfTabs, String course_id, Boolean isTeacher, String teacherName, int numberOfLessons) {
        super(fm);
        this.numOfTabs = numOfTabs;
        this.course_id = course_id;
        this.isTeacher = isTeacher;
        this.teacherName = teacherName;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CourseDetailFragment.newInstance(course_id, isTeacher, teacherName);
            case 1:
                return LessonListFragment.newInstance(isTeacher, course_id);
            case 2:
                return MemberListFragment.newInstance(course_id, numberOfLessons);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

}
