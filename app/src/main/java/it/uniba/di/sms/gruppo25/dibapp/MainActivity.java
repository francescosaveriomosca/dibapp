package it.uniba.di.sms.gruppo25.dibapp;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {

    ImageView bg;
    Button btnsign, btnlog;
    String UID;
    Animation frombottom, fromtop;
    TextView textView7, textView8;
    ProgressBar progressBar;
    Boolean teacher;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAuth();
    }

    private void checkAuth(){
        setContentView(R.layout.splash);

        textView7= (TextView)findViewById(R.id.textView7);
        textView8= (TextView)findViewById(R.id.textView8) ;
        progressBar= (ProgressBar)findViewById(R.id.progressBar);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);


        textView7.startAnimation(fromtop);
        textView8.startAnimation(fromtop);
        progressBar.startAnimation(frombottom);

       //checkIfEmailVerified();    //Attivazione verifica email


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            UID=user.getUid();

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference().child("users").child(UID).child("teacher");


            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    teacher= Boolean.parseBoolean(dataSnapshot.getValue().toString());
                    editor = getSharedPreferences("dib", MODE_PRIVATE).edit();
                    editor.putBoolean("isTeacher", teacher);
                    editor.apply();
                    Intent intent = new Intent(getBaseContext(), MainNavActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

        else {
            setContentView(R.layout.activity_main);

            bg = (ImageView) findViewById(R.id.bg);
            btnsign = (Button) findViewById(R.id.btnsign);
            btnlog = (Button) findViewById(R.id.btnlog);
            textView7 = (TextView) findViewById(R.id.textView2);
            textView8 = (TextView) findViewById(R.id.txt_nav_user_email);


            btnsign.startAnimation(frombottom);
            btnlog.startAnimation(frombottom);
            textView8.startAnimation(fromtop);
            textView7.startAnimation(fromtop);


            btnsign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(MainActivity.this, SignUpActivity.class);
                    startActivity(a);
                }
            });

            btnlog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(a);
                }
            });
        }
    }

    private void checkIfEmailVerified() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if (user.isEmailVerified()) {
                Toast.makeText(MainActivity.this, R.string.checkMailTrue, Toast.LENGTH_SHORT).show();
            } else {
               Toast.makeText(getApplicationContext(), R.string.checkMailfalse, Toast.LENGTH_SHORT).show();
                FirebaseAuth.getInstance().signOut();
            }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.alert_exit_title)
                .setMessage(R.string.alert_exit_message)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        ActivityCompat.finishAffinity(MainActivity.this);
                        finish();

                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
        dialog.show();
    }
}
