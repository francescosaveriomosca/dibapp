package it.uniba.di.sms.gruppo25.dibapp.model;

public class Comment {
    String comment;
    Boolean privato;
    Boolean isTeacher;

    public Comment(){

    }

    public Comment(String comment, Boolean privato, Boolean isTeacher){
        this.comment=comment;
        this.privato=privato;
        this.isTeacher=isTeacher;
    }

    public String getComment() {
        return comment;
    }

    public Boolean getPrivato() {
        return privato;
    }

    public Boolean getTeacher() {
        return isTeacher;
    }
}
