package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.util.Date;

import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.model.Lesson;

public class LessonListAdapter extends FirebaseRecyclerAdapter<Lesson, LessonViewHolder> {
    private Context context;
    private String courseId;
    private boolean isTeacher;

    protected LessonListAdapter(FirebaseRecyclerOptions<Lesson> options, Context context, String courseId, boolean isTeacher) {
        super(options);
        this.context = context;
        this.courseId = courseId;
        this.isTeacher = isTeacher;

    }

    @Override
    protected void onBindViewHolder(@NonNull LessonViewHolder holder, int position, @NonNull final Lesson model) {
        long now = new Date().getTime();
        if (!model.description.isEmpty())
            holder.setTxtDesc(model.description);
        holder.setTxtDate(model.date);
        //holder.setTxtStart(String.format(context.getResources().getString(R.string.starts_at), model.startTime));
        //holder.setTxtDuration(String.format(context.getResources().getString(R.string.ends_at), model.endTime));
        String status = context.getResources().getString(R.string.lesson_status_scheduled);
        if (model.ended) {
            status = context.getResources().getString(R.string.lesson_status_ended);
        } else if (model.started) {
            status = context.getResources().getString(R.string.lesson_status_started);
        }
         holder.setTxtStatus(String.format(context.getResources().getString(R.string.lesson_status), status));


        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LessonDetailActivity.class);
                intent.putExtra("lessonId", model.id);
                intent.putExtra("courseId", courseId);
                context.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lesson_list_item, parent, false);

        return new LessonViewHolder(view);
    }
}
