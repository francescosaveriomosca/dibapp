package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.model.User;

public class MemberListFragment extends Fragment {

    private static final String COURSE_ID = "course_id";
    private static final String N_OF_LESSONS = "numberOfLessons";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseRecyclerAdapter adapter;
    private FirebaseDatabase db;
    private String courseId;
    private int numberOfLessons;

    public static MemberListFragment newInstance(String courseId, int numberOfLessons) {
        MemberListFragment fragment = new MemberListFragment();
        Bundle args = new Bundle();
        args.putString(COURSE_ID, courseId);
        args.putInt(N_OF_LESSONS, numberOfLessons);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_member_list, container, false);
        courseId = getArguments().getString(COURSE_ID);
        //numberOfLessons = getArguments().getInt(N_OF_LESSONS);
        FirebaseDatabase.getInstance().getReference("lessons").child(courseId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                numberOfLessons = (int) dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        recyclerView = view.findViewById(R.id.rcv_member_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        fetch(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void fetch(final View view) {
        Query query = db.getInstance().getReference("courses").child(courseId).child("members");
        DatabaseReference ref = db.getInstance().getReference("users");
        FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>()
                .setIndexedQuery(query, ref, User.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<User, UserViewHolder>(options) {

            @NonNull
            @Override
            public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.member_list_item, parent, false);
                return  new UserViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final UserViewHolder holder, int position, @NonNull User model) {
                holder.setTxtName(model.name);
                holder.setTxtMatricola(model.matricola);
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("courses").child(courseId).child("members").child(model.id);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        int nOfPresences = Integer.valueOf(dataSnapshot.getValue().toString());
                        holder.setTxtPresences(String.format("%s: %d/%d", getString(R.string.presences), nOfPresences, numberOfLessons));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                view.findViewById(R.id.member_list_progress).setVisibility(View.GONE);
                if (getItemCount() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    TextView textView = view.findViewById(R.id.txt_empty_member_list);
                    textView.setText(getString(R.string.empty_member_list));
                    textView.setVisibility(View.VISIBLE);
                }
            }
        };

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
    */
}
