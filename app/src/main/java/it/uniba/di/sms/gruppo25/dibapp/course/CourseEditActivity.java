package it.uniba.di.sms.gruppo25.dibapp.course;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.model.Course;

public class CourseEditActivity extends AppCompatActivity {

    private EditText etxtName, etxtDesc, etxtPass;
    private LinearLayout linearLayout;
    private FirebaseAuth auth;
    private FirebaseDatabase db;
    private Intent intent;
    private boolean new_course;
    Animation frombottom, fromtop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_edit);
        intent = getIntent();
        new_course = true;
        etxtName = findViewById(R.id.etxt_course_name);
        etxtDesc = findViewById(R.id.etxt_course_desc);
        etxtPass = findViewById(R.id.etxt_course_pass);
        linearLayout = findViewById(R.id.linearEditCourse);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        if (intent.getStringExtra("courseId") != null){
            etxtName.setText(intent.getStringExtra("courseName"));
            etxtDesc.setText(intent.getStringExtra("courseDesc"));
            etxtPass.setText(intent.getStringExtra("coursePass"));
            new_course = false;
        }
        else {
            setTitle(getString(R.string.title_Activity_course_Create));
        }
        Button btnSave = findViewById(R.id.btn_course_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name, desc, pass;
                name = etxtName.getText().toString();
                desc = etxtDesc.getText().toString();
                pass = etxtPass.getText().toString();
                if (new_course) {
                    saveCourse(name, desc, pass);
                }
                else {
                    String id = intent.getStringExtra("courseId");
                    editCourse(id, name, desc, pass);
                }
            }
        });

        etxtDesc.startAnimation(fromtop);
        etxtName.startAnimation(fromtop);
        etxtPass.startAnimation(fromtop);
        linearLayout.startAnimation(fromtop);
        btnSave.startAnimation(frombottom);

    }

    private void saveCourse(String name, String desc, String pass) {
        DatabaseReference ref = db.getInstance().getReference("courses").push().getRef();
        auth = FirebaseAuth.getInstance();
        String id = ref.getKey();
        String teacherId = auth.getUid();
        Course course = new Course(id, name, desc, pass, teacherId, 0);
        if (validateCourse(course)) {
            ref.setValue(course);
            db.getInstance().getReference("users").child(auth.getUid()).child("courses").child(course.id).setValue(true);
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    private void editCourse(String id, String name, String desc, String pass) {
        DatabaseReference ref = db.getInstance().getReference("courses").child(id);
        auth = FirebaseAuth.getInstance();
        String teacherId = auth.getUid();
        Course course = new Course(id, name, desc, pass, teacherId, 0);
        if (validateCourse(course)) {
            ref.child("name").setValue(course.name);
            ref.child("description").setValue(course.description);
            ref.child("password").setValue(course.password);
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    private boolean validateCourse(Course course) {
        boolean isValid = true;
        if (course.name.length() < 3) {
            isValid = false;
            Snackbar.make(findViewById(R.id.course_edit_coord_layout), R.string.error_course_name, Snackbar.LENGTH_LONG).show();
        }
        return isValid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

}
