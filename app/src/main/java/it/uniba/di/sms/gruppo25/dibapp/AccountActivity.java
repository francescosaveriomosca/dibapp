package it.uniba.di.sms.gruppo25.dibapp;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AccountActivity extends AppCompatActivity {


    private EditText editName, editMatricola;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        getSupportActionBar().setTitle(R.string.title_activity_account_edit);

    }

    public void fabName(final View view){
        editName=(EditText) findViewById(R.id.editName);
        if(editName.getText().toString().isEmpty()){
            Snackbar.make(view, R.string.nameError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
        else{
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("name");
            ref.setValue(editName.getText().toString());
            Snackbar.make(view, R.string.nameOk , Snackbar.LENGTH_LONG).setAction("Action", null).show();
            editName.setText("");
        }
    }

    public void fabMatricola(final View view){
        editMatricola=(EditText) findViewById(R.id.editMatricola);
        if(editMatricola.getText().toString().isEmpty()){
            Snackbar.make(view, R.string.matricolaError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
        else{
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("matricola");
            ref.setValue(editMatricola.getText().toString());
            Snackbar.make(view, R.string.matricolaOk , Snackbar.LENGTH_LONG).setAction("Action", null).show();
            editMatricola.setText("");
        }
    }

    public void changePassword(final View v){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.sendPasswordResetEmail(user.getEmail())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Snackbar.make(v ,R.string.sendMailComplete , Snackbar.LENGTH_LONG).setAction("Action", null).show();

                        } else {
                            Snackbar.make(v ,R.string.sendMailFail , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
