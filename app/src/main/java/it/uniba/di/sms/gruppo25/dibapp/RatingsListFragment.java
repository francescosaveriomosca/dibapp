package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.FirebaseDatabase;

import it.uniba.di.sms.gruppo25.dibapp.model.Rating;

import static android.content.Context.MODE_PRIVATE;

public class RatingsListFragment extends Fragment {
    private static final String LESSON_ID = "lesson_id";

    private String lesson_id;
    private FirebaseListAdapter<Rating> adapter;
    private TextView textView;
    private RatingBar ratingBar;
    private Boolean isTeacher;
    FirebaseListOptions<Rating> opitons;

    public RatingsListFragment() {
        // Required empty public constructor
    }

    public static RatingsListFragment newInstance(String lesson_id) {
        Bundle args = new Bundle();
        RatingsListFragment fragment = new RatingsListFragment();
        args.putString(LESSON_ID, lesson_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        final View view = inflater.inflate(R.layout.fragment_ratings_list, container, false);

        lesson_id = getArguments().getString(LESSON_ID);

        ListView listOfComment = view.findViewById(R.id.listofratings);
        if(isTeacher){
            opitons = new FirebaseListOptions.Builder<Rating>()
                    .setLayout(R.layout.content_rating_list)
                    .setQuery(FirebaseDatabase.getInstance().getReference("ratings").child(lesson_id), Rating.class)
                    .build();
        }
        else{
            opitons = new FirebaseListOptions.Builder<Rating>()
                    .setLayout(R.layout.content_rating_list)
                    .setQuery(FirebaseDatabase.getInstance().getReference("ratings").child(lesson_id).orderByChild("privato").equalTo(false), Rating.class)
                    .build();
        }

        adapter = new FirebaseListAdapter<Rating>(opitons) {
            @Override
            protected void populateView(View v, Rating model, int position) {

                textView = v.findViewById(R.id.commentOutput);
                ratingBar = v.findViewById(R.id.ratingBarOutput);

                textView.setText(model.getRatingComment());
                ratingBar.setRating(model.getRating());
            }
            @Override
            public void onDataChanged() {
                super.onDataChanged();
                TextView textView = view.findViewById(R.id.txt_empty_lesson_list2);
                if (getCount() == 0) {

                    textView.setText(R.string.empty_ratings);
                    textView.setVisibility(View.VISIBLE);
                }
                else {
                    textView.setVisibility(View.GONE);
                }
            }
        };

        listOfComment.setAdapter(adapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
