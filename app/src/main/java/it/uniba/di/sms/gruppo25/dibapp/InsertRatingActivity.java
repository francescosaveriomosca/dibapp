package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Switch;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import it.uniba.di.sms.gruppo25.dibapp.model.Rating;

public class InsertRatingActivity extends AppCompatActivity {

    private RatingBar ratingBar;
    private EditText textInputEditText;
    private Button button;
    Rating rating;
    FirebaseUser user;
    FirebaseDatabase database;
     Switch switchPrivacy;
    private String lessonId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_rating);

        Intent intent = getIntent();
        lessonId = intent.getStringExtra("lessonId");


        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        textInputEditText = (EditText) findViewById(R.id.editText45);
        button = (Button) findViewById(R.id.sendRating);
        switchPrivacy = (Switch)findViewById(R.id.switchPrivacy2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = FirebaseAuth.getInstance().getCurrentUser();

                rating = new Rating(ratingBar.getRating(), textInputEditText.getText().toString(), user.getUid(), switchPrivacy.isChecked());


                database = FirebaseDatabase.getInstance();
                DatabaseReference ref = database.getReference("ratings").child(lessonId).child(user.getUid());
                ref.setValue(rating);

                finish();

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
