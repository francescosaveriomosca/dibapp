package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.C;
import it.uniba.di.sms.gruppo25.dibapp.InsertRatingActivity;
import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.StartLessonActivity;
import it.uniba.di.sms.gruppo25.dibapp.model.Lesson;



import static android.content.Context.MODE_PRIVATE;


public class LessonDetailFragment extends Fragment {

    private static final String COURSE_ID = "course_id";
    private static final String LESSON_ID = "lesson_id";

    private String lesson_id;
    private String course_id;
    private boolean isTeacher, isPresent;
    private Lesson lesson;

    private TextView txtDesc, txtDate, txtClassroom, txtStart, txtEnd, txtStatus, txtAttendance;
    private Button btnJoin, btnInsertRating, btnLessonOn, btnLessonOff;
    private RatingBar ratingBar;
    Context mContext;


    public LessonDetailFragment() {
        // Required empty public constructor
    }

    public static LessonDetailFragment newInstance(String course_id, String lesson_id) {
        LessonDetailFragment fragment = new LessonDetailFragment();
        Bundle args = new Bundle();
        args.putString(COURSE_ID, course_id);
        args.putString(LESSON_ID, lesson_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        mContext=getContext();

        final View view = inflater.inflate(R.layout.fragment_lesson_detail, container, false);
        view.findViewById(R.id.lesson_detail_content).setVisibility(View.GONE);
        view.findViewById(R.id.lesson_detail_progress).setVisibility(View.VISIBLE);
        txtDesc = view.findViewById(R.id.txt_lesson_desc);
        txtDate = view.findViewById(R.id.txt_lesson_date);
        txtStart = view.findViewById(R.id.txt_lesson_start);
        txtEnd = view.findViewById(R.id.txt_lesson_end);
        txtStatus = view.findViewById(R.id.txt_lesson_status);
        txtClassroom = view.findViewById(R.id.txt_lesson_classroom);
        txtAttendance = view.findViewById(R.id.txt_lesson_attendance);
        btnLessonOn = view.findViewById(R.id.btnLessonOn);
        btnLessonOff = view.findViewById(R.id.btnLessonOff);
        btnJoin = view.findViewById(R.id.btnJoin);

        course_id = getArguments().getString(COURSE_ID);
        lesson_id = getArguments().getString(LESSON_ID);

        retrieveLessonDetail(view, course_id, lesson_id, getContext());

        if (isTeacher) {
            btnLessonOn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activateLesson(view);
                }
            });
            btnLessonOff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopLesson(view);
                }
            });
            btnLessonOff.setVisibility(View.VISIBLE);
            btnLessonOn.setVisibility(View.VISIBLE);
        }
        else {

            btnJoin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    joinLesson(view);
                }
            });
            btnInsertRating = view.findViewById(R.id.btnInsertRating);
            btnInsertRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    insertRating(view);
                }
            });
            btnJoin.setVisibility(View.VISIBLE);
            btnInsertRating.setVisibility(View.VISIBLE);
            txtAttendance.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    private void retrieveLessonDetail(final View view, String courseId, String lessonId, final Context context) {
        final DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReference("lessons")
                .child(courseId)
                .child(lessonId);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    view.findViewById(R.id.lesson_detail_progress).setVisibility(View.GONE);
                    view.findViewById(R.id.lesson_detail_content).setVisibility(View.VISIBLE);
                    lesson = dataSnapshot.getValue(Lesson.class);
                    txtDate.setText(lesson.date);
                    // setTitle(lesson.date);
                    txtDesc.setText(context.getString(R.string.lesson_description, lesson.description));
                    txtClassroom.setText(context.getString(R.string.lesson_classroom, lesson.classroom));
                    txtStart.setText(context.getString(R.string.lesson_start, lesson.startTime));
                    txtEnd.setText(context.getString(R.string.lesson_end, lesson.endTime));
                    txtStatus.setText(context.getString(R.string.lesson_status, context.getString(R.string.lesson_status_scheduled)));
                    if (lesson.started) {
                        btnLessonOn.setVisibility(View.GONE);
                        if (isTeacher) btnLessonOff.setVisibility(View.VISIBLE);
                        txtStatus.setText(context.getString(R.string.lesson_status, context.getString(R.string.lesson_status_started)));
                    } else {
                        btnLessonOff.setVisibility(View.GONE);
                    }
                    if (lesson.ended) {
                        btnLessonOff.setVisibility(View.GONE);
                        txtStatus.setText(context.getString(R.string.lesson_status, context.getString(R.string.lesson_status_ended)));
                        btnJoin.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        retrievePresence(view, context);
    }

    private void retrievePresence(final View view, final Context context) {
        if (!isTeacher) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lesson_presences").child(lesson_id).child(user.getUid()).child("confirmed");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        if (dataSnapshot.getValue(Boolean.class) == true) {
                            txtAttendance.setText(context.getString(R.string.lesson_attendance_true));
                            btnJoin.setVisibility(View.GONE);
                            btnInsertRating.setVisibility(View.VISIBLE);
                        }
                    } else {
                        txtAttendance.setText(context.getString(R.string.lesson_attendance_false));
                        btnInsertRating.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public void activateLesson(final View view){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id).child("started");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Boolean.class)){
                   Snackbar.make(view ,R.string.lessonalreadybegun, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
                else{
                    Intent intent = new Intent(getContext(), StartLessonActivity.class);
                    intent.putExtra("lessonId", lesson_id);
                    intent.putExtra("courseId", course_id);
                    intent.putExtra("isTeacher", isTeacher);
                    startActivityForResult(intent, 1);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public static void setSnackBar(View root, String snackTitle) {
        Snackbar snackbar = Snackbar.make(root, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }



    public void insertRating(final View view){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id).child("ended");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.getValue(Boolean.class)){
                    Snackbar.make(view ,R.string.lessonNotEnd, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
                else{
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lesson_presences").child(lesson_id).child(user.getUid()).child("confirmed");
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue(Boolean.class) != null) {
                                if (dataSnapshot.getValue(Boolean.class)) {
                                    Intent intent = new Intent(getContext(), InsertRatingActivity.class);
                                    intent.putExtra("lessonId", lesson_id);
                                    startActivity(intent);
                                }
                                else{
                                    Snackbar.make(view ,R.string.Notpresent, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                }
                            }
                            else{
                                Snackbar.make(view ,R.string.Notpresent, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void joinLesson(final View view){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id).child("ended");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Boolean.class)) {
                    Snackbar.make(view, R.string.lessonStopped, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id).child("started");
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (!dataSnapshot.getValue(Boolean.class)) {
                                Snackbar.make(view, R.string.lessonNotStart, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            } else {
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lesson_presences").child(lesson_id).child(user.getUid()).child("confirmed");
                                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue(Boolean.class) != null) {
                                            if (dataSnapshot.getValue(Boolean.class)) {
                                                Snackbar.make(view, R.string.presenceAlredyTake, Snackbar.LENGTH_LONG).setAction("Action", null).show();

                                            } else {
                                                Intent intent = new Intent(getContext(), StartLessonActivity.class);
                                                intent.putExtra("lessonId", lesson_id);
                                                intent.putExtra("courseId", course_id);
                                                intent.putExtra("isTeacher", isTeacher);
                                                startActivityForResult(intent, 2);

                                            }
                                        } else {
                                            Intent intent = new Intent(getContext(), StartLessonActivity.class);
                                            intent.putExtra("lessonId", lesson_id);
                                            intent.putExtra("courseId", course_id);
                                            intent.putExtra("isTeacher", isTeacher);
                                            startActivityForResult(intent, 2);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
       }

    public void stopLesson(final View view){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id).child("started");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.getValue(Boolean.class)){
                    Snackbar.make(view ,R.string.lessonNotStart, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
                else{
                    FirebaseDatabase database;
                    database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef;
                    myRef = database.getReference("lessons").child(course_id).child(lesson_id).child("ended");
                    myRef.setValue(true);
                    Snackbar.make(view ,R.string.lessonStopped, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(getView().findViewById(R.id.lescord), getResources().getString(R.string.lessonOk), Snackbar.LENGTH_LONG).show();

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Snackbar.make(getView().findViewById(R.id.lescord), getResources().getString(R.string.lessonNo), Snackbar.LENGTH_LONG).show();
            }
        }

        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(getView().findViewById(R.id.lescord), getResources().getString(R.string.presenceok), Snackbar.LENGTH_LONG).show();

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Snackbar.make(getView().findViewById(R.id.lescord), getResources().getString(R.string.presenceerror), Snackbar.LENGTH_LONG).show();
            }
        }
    }



}

