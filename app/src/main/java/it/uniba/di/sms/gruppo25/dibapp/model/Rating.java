package it.uniba.di.sms.gruppo25.dibapp.model;

public class Rating {
    private float rating;
    private String ratingComment;
    private String UserId;
    Boolean privato;

    public Rating(){

    }

    public Rating(float rating, String ratingComment, String UserID, Boolean privato){
        this.rating = rating;
        this.ratingComment = ratingComment;
        this.UserId= UserID;
        this.privato=privato;
    }

    public float getRating() {
        return rating;
    }

    public String getRatingComment() {
        return ratingComment;
    }

    public String getUserId() {
        return UserId;
    }

    public Boolean getPrivato() {
        return privato;
    }
}
