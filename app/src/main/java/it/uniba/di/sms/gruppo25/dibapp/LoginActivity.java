package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Query;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Animation frombottom, fromtop;
    TextView textView;
    EditText mail, password;
    Button sign, reset;
    FirebaseAuth auth;
    FirebaseUser user;
    LinearLayout linearLayout;
    String pw, email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        textView = (TextView) findViewById(R.id.textView3);
        mail = (EditText) findViewById(R.id.emailTxt);
        password = (EditText) findViewById(R.id.passwordTxt);
        sign = (Button) findViewById(R.id.btnSign);
        reset = (Button) findViewById(R.id.resetBtn);
        linearLayout=(LinearLayout) findViewById(R.id.linearLogin);

        sign.startAnimation(frombottom);
        textView.startAnimation(fromtop);
        mail.startAnimation(fromtop);
        password.startAnimation(fromtop);
        reset.startAnimation(fromtop);
        linearLayout.startAnimation(fromtop);


        auth = FirebaseAuth.getInstance();


        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                email = mail.getText().toString();
                pw= password.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Snackbar.make( v,R.string.emailError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }
                if (TextUtils.isEmpty(pw)) {
                    Snackbar.make( v,R.string.pwError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }


                auth.signInWithEmailAndPassword(email,pw).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {
                            if (pw.length() < 6) {
                                Snackbar.make(v ,R.string.pwNumberError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            } else {
                                Snackbar.make( v,R.string.authFail , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }
                        } else {

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}
