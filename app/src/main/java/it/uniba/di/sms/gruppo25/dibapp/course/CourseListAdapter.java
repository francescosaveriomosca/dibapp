package it.uniba.di.sms.gruppo25.dibapp.course;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.model.Course;

public class CourseListAdapter extends FirebaseRecyclerAdapter<Course, CourseViewHolder> {
    private String filter;
    private Context context;
    private FragmentManager fragmentManager;
    private boolean isTeacher;

    CourseListAdapter(FirebaseRecyclerOptions<Course> options, Context context, FragmentManager fragmentManager, boolean isTeacher, String filter) {
        super(options);
        this.filter = filter;
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.isTeacher = isTeacher;

    }

    void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    protected void onBindViewHolder(@NonNull final CourseViewHolder holder, int position, @NonNull final Course model) {
        holder.setTxtTitle(model.name);
       // if (!model.description.isEmpty())
         //   holder.setTxtDesc(model.description);
      //  else
        //    holder.setTxtDesc(holder.root.getResources().getString(R.string.no_description_provided));
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        db.getReference("users").child(model.teacherId).child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               final String teacher = dataSnapshot.getValue().toString();
               // holder.setTxtTeacher(teacher);
                if (model.name.toLowerCase().contains(filter.toLowerCase()) || model.description.toLowerCase().contains(filter.toLowerCase())) {
                    holder.show();
                    holder.root.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, CourseDetailActivity.class);
                            intent.putExtra("course_id", model.id);
                            intent.putExtra("teacher_name", teacher);
                            intent.putExtra("numberOfLessons", model.numberOfLessons);
                            context.startActivity(intent);
                        }
                    });
                }
                else {
                    holder.hide();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @NonNull
    @Override
    public CourseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.course_list_item, parent, false);

        return new CourseViewHolder(view);
    }

}
