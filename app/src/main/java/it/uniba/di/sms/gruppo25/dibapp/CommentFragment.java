package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.lesson.LessonDetailActivity;
import it.uniba.di.sms.gruppo25.dibapp.model.Comment;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;

public class CommentFragment extends Fragment {

    private static final String LESSON_ID = "lesson_id";

    private String lesson_id;
    private FirebaseListAdapter<Comment> adapter;
    private Boolean isTeacher;
    private Switch switchPrivacy;
    private Boolean isPresent;
     View view;

    public static CommentFragment newInstance(String lesson_id) {


        Bundle args = new Bundle();
        args.putString(LESSON_ID, lesson_id);
        CommentFragment fragment = new CommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        lesson_id = getArguments().getString(LESSON_ID);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);
        
        view = inflater.inflate(R.layout.fragment_comment, container, false);

        if(isTeacher){   displayComment(view); }
        else{   comment(view);}



        return view;
    }



    private void displayComment(final View view){


        switchPrivacy = view.findViewById(R.id.switchPrivacy);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        switchPrivacy.setVisibility(GONE);
        ListView listOfComment = view.findViewById(R.id.listOfCommentsPrivate);

        FirebaseListOptions<Comment> opitons = new FirebaseListOptions.Builder<Comment>()
                .setLayout(R.layout.content_comment_list)
                .setQuery(FirebaseDatabase.getInstance().getReference().child("comment").child(lesson_id), Comment.class)  //al posto di lessonId "12edxs"
                .build();

        adapter = new FirebaseListAdapter<Comment>(opitons) {
            @Override
            protected void populateView(View v, Comment model, int position) {
                TextView messageText = v.findViewById(R.id.message_text);
                messageText.setText(model.getComment());
                if(model.getTeacher()){
                    TextView teacher = v.findViewById(R.id.account);
                    teacher.setText(R.string.teacher);
                }
                else{
                    TextView teacher = v.findViewById(R.id.account);
                    teacher.setText(R.string.anonimo);
                }
            }
        };

        listOfComment.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {

                EditText input = view.findViewById(R.id.input);
                if(input.getText().toString().isEmpty()){

                }
                else {

                    FirebaseDatabase.getInstance()
                            .getReference().child("comment").child(lesson_id)
                            .push()
                            .setValue(new Comment(input.getText().toString(), false, true)
                            );

                    input.setText("");
                }
            }
        });
    }

    private void comment(final View view){
        switchPrivacy = view.findViewById(R.id.switchPrivacy);
        final FloatingActionButton fab = view.findViewById(R.id.fab);
        final EditText input = view.findViewById(R.id.input);
        final LinearLayout linearLayout = view.findViewById(R.id.linearLogin);



        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lesson_presences").child(lesson_id).child(user.getUid()).child("confirmed");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Boolean.class) != null) {
                    if (dataSnapshot.getValue(Boolean.class)) {
                        isPresent=true;
                        switchPrivacy.setVisibility(View.VISIBLE);
                        fab.show();
                        input.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view1) {
                                if(switchPrivacy.isChecked()){
                                    Snackbar.make(view, R.string.privateAlert , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                }
                                if(input.getText().toString().isEmpty()){

                                }
                                else {

                                    FirebaseDatabase.getInstance()
                                            .getReference().child("comment").child(lesson_id)
                                            .push()
                                            .setValue(new Comment(input.getText().toString(), switchPrivacy.isChecked(), false)
                                            );

                                    input.setText("");
                                }
                            }
                        });
                    }
                    else{
                        isPresent=false;
                        switchPrivacy.setVisibility(View.INVISIBLE);
                        fab.hide();
                        input.setVisibility(View.INVISIBLE);
                        linearLayout.setVisibility(View.INVISIBLE);

                    }
                }
                else{
                    isPresent=false;
                    switchPrivacy.setVisibility(View.INVISIBLE);
                    fab.hide();
                    input.setVisibility(View.INVISIBLE);
                    linearLayout.setVisibility(View.INVISIBLE);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        displayPublicComment(view);
    }

    public void displayPublicComment(final View view){
        ListView listOfComment = view.findViewById(R.id.listOfCommentsPrivate);

        FirebaseListOptions<Comment> opitons = new FirebaseListOptions.Builder<Comment>()
                .setLayout(R.layout.content_comment_list)
                .setQuery(FirebaseDatabase.getInstance().getReference().child("comment").child(lesson_id).orderByChild("privato").equalTo(false), Comment.class)
                .build();

        adapter = new FirebaseListAdapter<Comment>(opitons) {
            @Override
            protected void populateView(View v, Comment model, int position) {
                TextView messageText = v.findViewById(R.id.message_text);
                messageText.setText(model.getComment());
                if(model.getTeacher()){
                    TextView teacher = v.findViewById(R.id.account);
                    teacher.setText(R.string.teacher);
                }
                else{
                    TextView teacher = v.findViewById(R.id.account);
                    teacher.setText(R.string.anonimo);
                }
            }
        };

        listOfComment.setAdapter(adapter);
    }

    public static void setSnackBar(View root, String snackTitle) {
        Snackbar snackbar = Snackbar.make(root, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
    */
}

