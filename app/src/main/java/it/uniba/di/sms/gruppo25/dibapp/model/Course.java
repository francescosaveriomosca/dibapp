package it.uniba.di.sms.gruppo25.dibapp.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
final public class Course implements Serializable {
    public String id;
    public String name;
    public String description;
    public String password;
    public String teacherId;
    public int numberOfLessons;

    public Course() {}

    public Course(String id, String name, String description, String password, String teacherId, int numberOfLessons) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.password = password;
        this.teacherId = teacherId;
        this.numberOfLessons = numberOfLessons;
    }




}
