package it.uniba.di.sms.gruppo25.dibapp.course;

import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.uniba.di.sms.gruppo25.dibapp.R;

final public class CourseViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout root;
    public TextView txtTitle;
    public TextView txtDesc;
    public TextView txtTeacher;

    CourseViewHolder(View itemView) {
        super(itemView);
        root = itemView.findViewById(R.id.course_list_root);
        txtTitle = itemView.findViewById(R.id.course_list_title);

    }

    public void setTxtTitle(String string) {
        txtTitle.setText(string);
    }

    public void setTxtDesc(String string) {
        txtDesc.setText(string);
    }

    public void setTxtTeacher(String string) {txtTeacher.setText(string);}

    public void hide() {
        root.setVisibility(View.GONE);
        txtTitle.setVisibility(View.GONE);
        root.setPadding(0, 0, 0, 0);
    }

    public void show() {
        root.setVisibility(View.VISIBLE);
        txtTitle.setVisibility(View.VISIBLE);
        root.setPadding(0, 15, 0, 15 );
    }

}
