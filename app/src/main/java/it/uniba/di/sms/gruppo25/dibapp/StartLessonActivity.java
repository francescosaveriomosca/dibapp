package it.uniba.di.sms.gruppo25.dibapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.lesson.LessonDetailActivity;
import it.uniba.di.sms.gruppo25.dibapp.lesson.LessonDetailFragment;
import it.uniba.di.sms.gruppo25.dibapp.model.Position;
import it.uniba.di.sms.gruppo25.dibapp.model.Presence;
import it.uniba.di.sms.gruppo25.dibapp.model.User;

public class StartLessonActivity extends AppCompatActivity implements LocationListener {

    private TextView textView;
    private LocationManager locationManager;
    private FirebaseUser user;
    private DatabaseReference myRef;
    private User user1;
    private FirebaseDatabase database;
    private String lessonId, courseId;
    private Boolean isTeacher;
    private Position loc;
    private Presence presence;
    private Boolean accepted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        textView = findViewById(R.id.textView8);
        textView.setText(R.string.position_splash);

        Intent intent = getIntent();
        lessonId = intent.getStringExtra("lessonId");
        courseId = intent.getStringExtra("courseId");
        //isTeacher = intent.getBooleanExtra("isTeacher", true);

        SharedPreferences sharedPreferences = getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        getUser();
        CheckPermission();
        getLocation();

    }

    public void onResume(){
        super.onResume();
        setContentView(R.layout.splash);
        CheckPermission();
        getLocation();
    }

    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 4, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void CheckPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }


    }

    public void onProviderDisabled(String provider) {
        setSnackBar(findViewById(R.id.splash), getResources().getString(R.string.gpsOff));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        setSnackBar(findViewById(R.id.splash), getResources().getString(R.string.providerOn));


    }


    public void onLocationChanged(final Location location){
        loc = new Position(location.getAccuracy(), location.getLatitude(), location.getLongitude());
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("lessons").child(courseId).child(lessonId).child("position");
        if(isTeacher) {
            if (loc.getAccuracy() <= 50) {
                myRef.setValue(loc);
                myRef = database.getReference("lessons").child(courseId).child(lessonId).child("started");
                myRef.setValue(true);
                Intent intent = new Intent(StartLessonActivity.this, LessonDetailFragment.class);
                intent.putExtra("lessonId", lessonId);
                intent.putExtra("courseId", courseId);
                intent.putExtra("isTeacher", isTeacher);
                setResult(Activity.RESULT_OK);
                finish();
            }
            else{
                Intent intent = new Intent(StartLessonActivity.this, LessonDetailFragment.class);
                intent.putExtra("lessonId", lessonId);
                intent.putExtra("courseId", courseId);
                intent.putExtra("isTeacher", isTeacher);
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        }
        else{
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (loc.getAccuracy() <= 50) {
                        Position position = dataSnapshot.getValue(Position.class);
                        if (position.distanceTo(loc) < 35) {
                            setPresence();
                            Intent intent = new Intent(StartLessonActivity.this, LessonDetailFragment.class);
                            intent.putExtra("lessonId", lessonId);
                            intent.putExtra("courseId", courseId);
                            intent.putExtra("isTeacher", isTeacher);
                            setResult(Activity.RESULT_OK);
                            finish();

                        } else {
                            Intent intent = new Intent(StartLessonActivity.this, LessonDetailFragment.class);
                            intent.putExtra("lessonId", lessonId);
                            intent.putExtra("courseId", courseId);
                            intent.putExtra("isTeacher", isTeacher);
                            setResult(Activity.RESULT_CANCELED, intent);
                            finish();
                        }
                    }
                    else{
                        Intent intent = new Intent(StartLessonActivity.this, LessonDetailFragment.class);
                        intent.putExtra("lessonId", lessonId);
                        intent.putExtra("courseId", courseId);
                        intent.putExtra("isTeacher", isTeacher);
                        setResult(Activity.RESULT_CANCELED, intent);
                        finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public static void setSnackBar(View root, String snackTitle) {
        Snackbar snackbar = Snackbar.make(root, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }


    public void setPresence(){
        presence = new Presence(user1.name, user1.matricola, true);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("lesson_presences").child(lessonId).child(user.getUid());
        myRef.setValue(presence).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                final DatabaseReference ref = database.getReference("courses").child(courseId).child("members").child(user.getUid());
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        int nOfPresences = Integer.parseInt(dataSnapshot.getValue().toString());
                        nOfPresences++;
                        ref.setValue(nOfPresences);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    public void getUser(){
        user = FirebaseAuth.getInstance().getCurrentUser();

        String UID = user.getUid();
        database = FirebaseDatabase.getInstance();

        myRef = database.getReference("users").child(UID);


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user1 = dataSnapshot.getValue(User.class);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
