package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import it.uniba.di.sms.gruppo25.dibapp.model.Presence;

public class PresencesListFragment extends Fragment {

    private static final String LESSON_ID = "lesson_id";

    private FirebaseListAdapter<Presence> adapter;
    private FirebaseDatabase database;
    private DatabaseReference ref;
    private String lesson_id;

    public PresencesListFragment() {

    }

    public static PresencesListFragment newInstance(String lesson_id) {
        Bundle args = new Bundle();
        PresencesListFragment fragment = new PresencesListFragment();
        args.putString(LESSON_ID, lesson_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_presences_list, container, false);
        lesson_id = getArguments().getString(LESSON_ID);
        database = FirebaseDatabase.getInstance();
        ref = database.getReference("lesson_presences").child(lesson_id);   // ref = database.getReference("lessons_presences");
        list(view);
        return view;
    }


    public void list(final View view){
        ListView listView = view.findViewById(R.id.listofpresences2);
        FirebaseListOptions<Presence> options = new FirebaseListOptions.Builder<Presence>()
                .setLayout(R.layout.content_presence_list)
                .setQuery(ref, Presence.class)
                .build();

        adapter = new FirebaseListAdapter<Presence>(options) {
            @Override
            protected void populateView(@NonNull View v, @NonNull Presence model, int position) {
                TextView user_id = v.findViewById(R.id.user_id2);
                TextView user_pres = v.findViewById(R.id.user_presences2);

                user_id.setText("Matricola:" + model.getMatricola());
                user_pres.setText("Nome:" + model.getNome());
            }
            @Override
            public void onDataChanged() {
                super.onDataChanged();
                TextView textView = view.findViewById(R.id.txt_empty_lesson_list1);
                if (getCount() == 0) {

                    textView.setText(R.string.empty_presence);
                    textView.setVisibility(View.VISIBLE);
                }
                else {
                    textView.setVisibility(View.GONE);
                }
            }
        };
        listView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
    */

}
