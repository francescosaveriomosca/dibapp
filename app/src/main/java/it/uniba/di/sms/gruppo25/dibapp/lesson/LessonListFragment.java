package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import it.uniba.di.sms.gruppo25.dibapp.C;
import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.course.CourseDetailActivity;
import it.uniba.di.sms.gruppo25.dibapp.model.Lesson;

import static android.content.Context.MODE_PRIVATE;

public class LessonListFragment extends Fragment {

    private static final String IS_TEACHER = "isTeacher";
    private static final String COURSE_ID = "course_id";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private LessonListAdapter adapter;
    private FirebaseDatabase db;
    private String courseId;
    private boolean isTeacher;
    private String emptyListMessage;

    public LessonListFragment() {

    }

    public static LessonListFragment newInstance(boolean isTeacher, String courseId) {
        LessonListFragment fragment = new LessonListFragment();
        Bundle args = new Bundle();
        args.putString(COURSE_ID, courseId);
        args.putBoolean(IS_TEACHER, isTeacher);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_lesson_list, container, false);
        courseId = getArguments().getString(COURSE_ID);
        isTeacher = getArguments().getBoolean(IS_TEACHER);


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), LessonEditActivity.class);
                intent.putExtra("courseId", courseId);
                startActivityForResult(intent, C.NEW_LESSON_REQ);
            }
        });
        if (!isTeacher) {
            fab.hide();
            emptyListMessage = getString(R.string.empty_lesson_list_student_message);
        }
        else {
            emptyListMessage = getString(R.string.empty_lesson_list_teacher_messsage);
        }
        recyclerView = view.findViewById(R.id.rcv_lesson_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        fetch(view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void  onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == C.NEW_LESSON_REQ && resultCode == Activity.RESULT_OK) {
            View view = getView().findViewById(R.id.lesson_list_coord);
            Snackbar.make(view, getResources().getString(R.string.lesson_created), Snackbar.LENGTH_LONG).show();

        }
    }

    private void fetch(final View view) {
        view.findViewById(R.id.lesson_list_progress).setVisibility(View.VISIBLE);
        Query query = db.getInstance().getReference("lessons").child(courseId).orderByChild("startTimestamp");

        FirebaseRecyclerOptions<Lesson> options = new FirebaseRecyclerOptions.Builder<Lesson>()
                .setQuery(query, Lesson.class)
                .build();

        adapter = new LessonListAdapter(options, getContext(), courseId, isTeacher) {
            @Override
            public void onDataChanged() {
                super.onDataChanged();
                view.findViewById(R.id.lesson_list_progress).setVisibility(View.GONE);
                TextView textView = view.findViewById(R.id.txt_empty_lesson_list);
                if (getItemCount() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    textView.setText(emptyListMessage);
                    textView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    textView.setVisibility(View.GONE);
                }
            }
        };
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //getView().onBackPressed();
            return true;
        }
        return false;
    }
    */
}
