package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.uniba.di.sms.gruppo25.dibapp.R;

final public class LessonViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout root;
    public TextView txtDate;
    public TextView txtDesc;
    public TextView txtStatus;

    LessonViewHolder(View itemView) {
        super(itemView);
        root = itemView.findViewById(R.id.lesson_list_root);
        txtDate = itemView.findViewById(R.id.lesson_list_date);
        txtDesc = itemView.findViewById(R.id.lesson_list_desc);
        txtStatus = itemView.findViewById(R.id.lesson_list_status);
    }

    public void setTxtDate(String string) {
        txtDate.setText(string);
    }

    public void setTxtDesc(String string) {
        txtDesc.setText(string);
    }

    public void setTxtStatus(String string) {
       txtStatus.setText(string) ;
    }

    public void setColor(int color) {
        root.setBackgroundColor(color);
    }


}
