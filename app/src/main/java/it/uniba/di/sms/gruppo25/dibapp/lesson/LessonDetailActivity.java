package it.uniba.di.sms.gruppo25.dibapp.lesson;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import it.uniba.di.sms.gruppo25.dibapp.C;
import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.model.Lesson;
import static it.uniba.di.sms.gruppo25.dibapp.StartLessonActivity.setSnackBar;

public class LessonDetailActivity extends AppCompatActivity {
    private boolean isTeacher, isPresent;
    private String lesson_id;
    private String course_id;
    private Lesson lesson;
    PagerAdapter adapter;
    AppBarLayout appBarLayout;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_detail);

        Intent intent = getIntent();
        course_id = intent.getStringExtra("courseId");
        lesson_id = intent.getStringExtra("lessonId");

        SharedPreferences sharedPreferences = getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);
        appBarLayout = findViewById(R.id.lesson_detail_appbar);
        progressBar = findViewById(R.id.lesson_detail_progress);
        FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    lesson = dataSnapshot.getValue(Lesson.class);
                    getSupportActionBar().setTitle(lesson.description);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        TabLayout tabLayout = findViewById(R.id.lesson_tabs);
        final ViewPager viewPager = findViewById(R.id.lesson_view_pager);
        viewPager.setOffscreenPageLimit(4);
        if (isTeacher) {
            adapter = new LessonPagerAdapter(
                    getSupportFragmentManager(), tabLayout.getTabCount(), course_id, lesson_id);
        }
        else {
            tabLayout.removeTabAt(tabLayout.getTabCount()-1);
            adapter = new LessonPagerAdapter(
                    getSupportFragmentManager(), tabLayout.getTabCount(), course_id, lesson_id);
        }

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        progressBar.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isTeacher) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.detail_menu, menu);
            return true;
        }
        return  false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        else if (item.getItemId() == R.id.edit) {
            if (lesson.started) {
                Snackbar.make(findViewById(R.id.lesson_detail_coord), R.string.error_lesson_edit, Snackbar.LENGTH_LONG ).show();
            }
            else {
                Intent intent = new Intent(this, LessonEditActivity.class);
                intent.putExtra("lessonDesc", lesson.description);
                intent.putExtra("lessonDate", lesson.date);
                intent.putExtra("lessonClass", lesson.classroom);
                intent.putExtra("lessonStart", lesson.startTime);
                intent.putExtra("lessonEnd", lesson.endTime);
                intent.putExtra("courseId", course_id);
                intent.putExtra("lessonId", lesson.id);
                startActivityForResult(intent, C.EDIT_LESSON_REQ);
            }
        }
        else if (item.getItemId() == R.id.delete) {
            if (lesson.started) {
                Snackbar.make(findViewById(R.id.lesson_detail_coord), R.string.error_lesson_delete, Snackbar.LENGTH_LONG ).show();
            }
            else {
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle(R.string.alert_delete_lesson_title)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("lessons").child(course_id).child(lesson_id);
                                ref.removeValue();

                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .create();
                dialog.show();
            }
        }
        return true;
    }




}
