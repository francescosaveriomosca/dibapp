package it.uniba.di.sms.gruppo25.dibapp.model;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Lesson {
    public String id;
    public String classroom;
    public String description;
    public String date;
    public String startTime;
    public String endTime;
    public long startTimestamp;
    public long endTimestamp;
    public boolean started;
    public boolean ended;
    public Position position;
    public long createdAt;

    public Lesson() {

    }

    public Lesson(String id, String classroom, String description, String date, String startTime, String endTime, boolean started, boolean ended, Position position) {
        this.id = id;
        this.classroom = classroom;
        this.description = description;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.started = started;
        this.ended = ended;
        this.position = position;
        this.startTimestamp = toTimeStamp(date, startTime);
        this.endTimestamp = toTimeStamp(date, endTime);
        this.createdAt = new Date().getTime();
    }

    private long toTimeStamp(String date, String time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        try {
            Date parsedDate = dateFormat.parse(String.format("%s %s", date, time));
            long timestamp = parsedDate.getTime();
            return timestamp;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
