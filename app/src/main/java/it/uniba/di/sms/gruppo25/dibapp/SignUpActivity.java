package it.uniba.di.sms.gruppo25.dibapp;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Query;

import android.text.TextUtils;
import android.view.View;

import it.uniba.di.sms.gruppo25.dibapp.model.User;


public class SignUpActivity extends AppCompatActivity {

    Animation frombottom, fromtop;
    Button signinbtn;
    TextView textView;
    EditText mailbt, namebt, passwordbt, matricolabt;
    String name, matricola, mail, password;
    Boolean teacher = false;
    FirebaseAuth auth;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        signinbtn = (Button) findViewById(R.id.btnLogin);
        textView = (TextView) findViewById(R.id.textView4);
        mailbt = (EditText) findViewById(R.id.mailTxt);
        namebt = (EditText) findViewById(R.id.nameTxt);
        passwordbt = (EditText) findViewById(R.id.pwTxt);
        matricolabt = (EditText) findViewById(R.id.matricolaTxt);
        linearLayout = (LinearLayout) findViewById(R.id.linearSignUp);

        signinbtn.startAnimation(frombottom);
        textView.startAnimation(fromtop);
        matricolabt.startAnimation(fromtop);
        namebt.startAnimation(fromtop);
        passwordbt.startAnimation(fromtop);
        mailbt.startAnimation(fromtop);
        linearLayout.startAnimation(fromtop);


        signinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                name = namebt.getText().toString();
                matricola = matricolabt.getText().toString();
                mail = mailbt.getText().toString();
                password = passwordbt.getText().toString();

                if(TextUtils.isEmpty(name)){
                    Snackbar.make(v ,R.string.nameError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }

                if(TextUtils.isEmpty(mail)){
                    Snackbar.make(v ,R.string.emailError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }

                if(TextUtils.isEmpty(matricola)){
                    Snackbar.make(v ,R.string.matricolaError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }

                if(password.length()<6){
                    Snackbar.make(v ,R.string.pwNumberError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }

                auth = FirebaseAuth.getInstance();

                auth.createUserWithEmailAndPassword(mail, password).addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Snackbar.make(v ,R.string.authFail , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else {
                            Snackbar.make(v ,R.string.signComplete , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();

                            User user1 = new User(user.getUid(), name, matricola, teacher);

                            DatabaseReference myRef = database.getReference("users").child(user.getUid());

                            myRef.setValue(user1);

                            sendVerificationEmail();
                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        }
                    }
                });
            }
        });
    }

    private void sendVerificationEmail()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            FirebaseAuth.getInstance().signOut();
                            finish();
                        }
                        else
                        {
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());

                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}
