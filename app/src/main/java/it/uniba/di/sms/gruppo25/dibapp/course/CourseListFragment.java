package it.uniba.di.sms.gruppo25.dibapp.course;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import it.uniba.di.sms.gruppo25.dibapp.C;
import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.model.Course;

import static android.content.Context.MODE_PRIVATE;


public class CourseListFragment extends Fragment {

    private static final String SHOW_ALL = "showAll";
    private static final String FILTER = "filter";
    private static final String IS_TEACHER = "isTeacher";

    private boolean showAll;
    private String filter;
    private boolean isTeacher;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CourseListAdapter adapter;
    private FirebaseDatabase db;
    private FloatingActionButton fabAddCourse;
    private String emptyListMessage;

    public CourseListFragment() {
        // Required empty public constructor
    }

    public static CourseListFragment newInstance(boolean isTeacher, boolean showAll, String filter) {
        CourseListFragment fragment = new CourseListFragment();
        Bundle args = new Bundle();
        args.putBoolean(SHOW_ALL, showAll);
        args.putString(FILTER, filter);
        args.putBoolean(IS_TEACHER, isTeacher);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            showAll = getArguments().getBoolean(SHOW_ALL);
            filter = getArguments().getString(FILTER);
           // isTeacher = getArguments().getBoolean(IS_TEACHER);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);

        View view = inflater.inflate(R.layout.fragment_course_list, container, false);
        recyclerView = view.findViewById(R.id.rcv_course_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        fetch();
        fabAddCourse = view.findViewById(R.id.fab_course_add);
        fabAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CourseEditActivity.class);
                startActivityForResult(intent, C.NEW_COURSE_REQ);
            }
        });
        if (showAll) {
            fabAddCourse.hide();
            emptyListMessage = getString(R.string.empty_all_course_list_message);
        }
        else {
            if (!isTeacher) {
                fabAddCourse.hide();
                emptyListMessage = getString(R.string.empty_course_list_student_message);

            }
            else {
                emptyListMessage = getString(R.string.empty_course_list_teacher_message);
            }
        }
        if (showAll) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.all_courses));
        }
        else {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.my_courses));
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    private void fetch() {
        Query query;
        DatabaseReference ref;
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseRecyclerOptions<Course> options;
        if (showAll) {
            query = FirebaseDatabase.getInstance().getReference("courses").orderByChild("name");
            options = new FirebaseRecyclerOptions.Builder<Course>()
                    .setQuery(query, Course.class)
                    .build();
        }
        else {
            query = FirebaseDatabase.getInstance()
                    .getReference("users")
                    .child(auth.getUid())
                    .child("courses");
            ref = FirebaseDatabase.getInstance().getReference("courses");

            options =  new FirebaseRecyclerOptions.Builder<Course>()
                            .setIndexedQuery(query, ref, Course.class)
                            .build();
        }

        adapter = new CourseListAdapter(options, getContext(), getActivity().getSupportFragmentManager(), isTeacher,"") {
            @Override
            public void onDataChanged() {
                super.onDataChanged();
                ProgressBar progressBar = getView().findViewById(R.id.course_list_progress);
                progressBar.setVisibility(View.GONE);
                TextView textView = getView().findViewById(R.id.txt_empty_course_list);
                if (getItemCount() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    textView.setText(emptyListMessage);
                    textView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    textView.setVisibility(View.GONE);
                }
            }
        };
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == C.NEW_COURSE_REQ && resultCode == Activity.RESULT_OK) {
            View view = getActivity().findViewById(R.id.coord_layout_course);
            Snackbar.make(view, getResources().getString(R.string.course_created), Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem search = menu.findItem(R.id.app_bar_search);

        SearchView searchView = (SearchView) search.getActionView();
        searchView.setQueryHint(getString(R.string.search));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.setFilter(newText);
                adapter.notifyDataSetChanged();
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                adapter.setFilter("");
                adapter.notifyDataSetChanged();
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }
}
