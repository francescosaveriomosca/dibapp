package it.uniba.di.sms.gruppo25.dibapp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout root;
    public TextView txtName;
    public TextView txtMatricola;
    public TextView txtPresences;

    UserViewHolder(View itemView) {
        super(itemView);
        root = itemView.findViewById(R.id.member_list_root);
        txtName = itemView.findViewById(R.id.member_list_name);
        txtMatricola = itemView.findViewById(R.id.member_list_matricola);
        txtPresences = itemView.findViewById(R.id.member_list_presences);
    }

    public void setTxtName(String string) {
        txtName.setText(string);
    }

    public void setTxtMatricola(String string) {
        txtMatricola.setText(string);
    }

    public void setTxtPresences(String string) { txtPresences.setText(string);}

}
