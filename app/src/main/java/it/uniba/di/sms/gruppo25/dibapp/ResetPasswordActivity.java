package it.uniba.di.sms.gruppo25.dibapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Query;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

public class ResetPasswordActivity extends AppCompatActivity {

    Button resetPwbt;
    TextView textView;
    EditText mailtxt;
    String mail;
    FirebaseAuth auth;
    LinearLayout linearLayout;
    Animation frombottom, fromtop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        resetPwbt= (Button) findViewById(R.id.btnresetpw);
        textView= (TextView) findViewById(R.id.textView5);
        mailtxt= (EditText) findViewById(R.id.passwordresettxt);
        linearLayout= (LinearLayout) findViewById(R.id.linearReset);

        linearLayout.startAnimation(fromtop);
        resetPwbt.startAnimation(frombottom);
        textView.startAnimation(fromtop);
        mailtxt.startAnimation(frombottom);

        auth = FirebaseAuth.getInstance();



        resetPwbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                mail = mailtxt.getText().toString().trim();

                if (TextUtils.isEmpty(mail)) {
                    Snackbar.make(v ,R.string.emailError , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return;
                }


                auth.sendPasswordResetEmail(mail)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Snackbar.make(v ,R.string.sendMailComplete , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                    startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));

                                } else {
                                    Snackbar.make(v ,R.string.sendMailFail , Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                }
                            }
                        });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
