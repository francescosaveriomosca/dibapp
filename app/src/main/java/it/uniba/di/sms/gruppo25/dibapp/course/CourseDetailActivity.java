package it.uniba.di.sms.gruppo25.dibapp.course;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo25.dibapp.C;
import it.uniba.di.sms.gruppo25.dibapp.R;
import it.uniba.di.sms.gruppo25.dibapp.course.CoursePagerAdapter;
import it.uniba.di.sms.gruppo25.dibapp.lesson.LessonEditActivity;
import it.uniba.di.sms.gruppo25.dibapp.model.Course;

public class CourseDetailActivity extends AppCompatActivity {

    String course_id;
    String teacher_name;
    Boolean isTeacher;
    int numberOfLessons;
    Course course;
    PagerAdapter adapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    AppBarLayout appBarLayout;
    ProgressBar courseDetailProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);
        Intent intent = getIntent();
        course_id = intent.getStringExtra("course_id");
        teacher_name = intent.getStringExtra("teacher_name");
        appBarLayout = findViewById(R.id.course_detail_appbar);
        courseDetailProgress = findViewById(R.id.course_detail_progress);
        // Retrieves number of lessons in the course
        FirebaseDatabase.getInstance().getReference("lessons").child(course_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                numberOfLessons = (int) dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        // Retrieves course stored details
        FirebaseDatabase.getInstance().getReference("courses").child(course_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                course = dataSnapshot.getValue(Course.class);
                // set title
                getSupportActionBar().setTitle(course.name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        SharedPreferences sharedPreferences = this.getSharedPreferences("dib", MODE_PRIVATE);
        isTeacher = sharedPreferences.getBoolean("isTeacher", false);
        // set up viewpager
        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(3);
        // adapt viewpager tabs based on user role and subscription
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(auth.getUid()).child("courses").child(course_id);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    findViewById(R.id.course_detail_appbar).setVisibility(View.VISIBLE);
                    if (isTeacher) {
                        adapter = new CoursePagerAdapter(
                                getSupportFragmentManager(), tabLayout.getTabCount(), course_id, isTeacher, teacher_name, numberOfLessons);
                    }
                    else {
                        if (tabLayout.getTabCount() == 3) {
                            tabLayout.removeTabAt(2);
                        }
                        else if (tabLayout.getTabCount() == 1) {
                            TabLayout.Tab tab = tabLayout.newTab();
                            tabLayout.addTab(tab);
                            tab.setText(R.string.lessons);
                        }
                        adapter = new CoursePagerAdapter(
                                getSupportFragmentManager(), 2, course_id, isTeacher, teacher_name, numberOfLessons);
                    }
                }
                else {
                    tabLayout.removeTabAt(1);
                    if (tabLayout.getTabCount() == 2) {
                        tabLayout.removeTabAt(1);
                    }
                    adapter = new CoursePagerAdapter(getSupportFragmentManager(), 1, course_id, isTeacher, teacher_name, numberOfLessons);
                }

                viewPager.setAdapter(adapter);
                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        courseDetailProgress.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isTeacher) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.detail_menu, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit) {
            Intent intent = new Intent(this, CourseEditActivity.class);
            intent.putExtra("courseId", course.id);
            intent.putExtra("courseName", course.name);
            intent.putExtra("courseDesc", course.description);
            intent.putExtra("coursePass", course.password);
            startActivityForResult(intent, C.EDIT_COURSE_REQ);
        }
        else if (item.getItemId() == R.id.delete) {
            if (numberOfLessons > 0) {
                Snackbar.make(findViewById(R.id.course_detail_coord), R.string.error_course_delete, Snackbar.LENGTH_LONG).show();
            }
            else {
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setTitle(R.string.alert_delete_course_content)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, CourseListFragment.newInstance(isTeacher, false, "")).commit();
                                FirebaseDatabase.getInstance().getReference("courses").child(course_id).removeValue();
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .create();
                alertDialog.show();
            }
        }
        else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void  onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == C.EDIT_COURSE_REQ && resultCode == Activity.RESULT_OK) {
            View view = findViewById(R.id.course_detail_coord);
            setSnackBar(view, getResources().getString(R.string.course_edited) );
        }

    }
    public static void setSnackBar(View root, String snackTitle) {
        Snackbar snackbar = Snackbar.make(root, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }


}
